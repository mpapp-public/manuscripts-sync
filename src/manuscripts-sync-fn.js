function syncFn(doc, oldDoc) {
  // VALIDATOR_FUNCTION

  var errorMessage;

  if (oldDoc && oldDoc._deleted && doc._deleted) {
    throw { forbidden: 'deleted document cannot be mutated' };
  }

  if (!doc._id) {
    throw { forbidden: 'missing _id' };
  }

  if (
    doc.objectType &&
    doc._id.substr(0, doc._id.indexOf(':')) !== doc.objectType
  ) {
    throw { forbidden: '_id must have objectType as prefix' };
  }

  var isAllowedToUndelete = !!doc.containerID;

  if (oldDoc && oldDoc._deleted && !isAllowedToUndelete) {
    // Only allow admin port to undelete documents for which isAllowedToUndelete is falsy.
    requireAdmin();

    errorMessage = validate(doc);

    if (errorMessage) {
      // prettier-ignore
      throw({ forbidden: errorMessage });
    }
  } else if (!doc._deleted || isAllowedToUndelete) {
    // check that the update isn't mutating objectType
    if (oldDoc && oldDoc.objectType !== doc.objectType) {
      // prettier-ignore
      throw({ forbidden: 'objectType cannot be mutated' });
    }

    if (oldDoc && oldDoc.containerID !== doc.containerID) {
      // prettier-ignore
      throw({ forbidden: 'containerID cannot be mutated' });
    }

    errorMessage = validate(doc);

    if (errorMessage) {
      // prettier-ignore
      throw({ forbidden: errorMessage });
    }
  }

  if (doc.locked || (oldDoc && oldDoc.locked)) {
    // Only allow admin port to create, modify or delete locked objects
    requireAdmin();
  }

  function objectTypeMatches(arg) {
    return doc.objectType === arg || (oldDoc && oldDoc.objectType === arg);
  }

  // Arrays are order aware
  /**
   * @param key string
   * @returns {boolean}
   */
  function hasMutated(key) {
    // when oldDoc === null, we're dealing with the first revision, which is never a mutation.
    if (oldDoc === null) return false;

    // records that cannot be deleted don't need to be checked for mutations at deletion time.
    if (doc._deleted && !isAllowedToUndelete) return false;

    if (key) {
      var objA = doc[key];
      var objB = oldDoc[key];
      return !equal(objA, objB);
    }

    return !equal(doc, oldDoc);
  }

  if (
    objectTypeMatches('MPProject') ||
    objectTypeMatches('MPLibraryCollection') ||
    objectTypeMatches('MPLibrary')
  ) {
    var owners, writers, viewers, annotators, editors;

    if (doc._deleted) {
      owners = oldDoc.owners;
      writers = oldDoc.writers;
      viewers = (oldDoc.viewers || []).filter(function(v) {
        return v !== '*';
      });
      annotators = oldDoc.annotators || [];
      editors = oldDoc.editors || [];
    } else {
      if (doc.owners.length === 0) {
        // prettier-ignore
        throw({ forbidden: 'owners cannot be set/updated to be empty' });
      }
      owners = doc.owners;
      writers = doc.writers;
      // filter out the public access marking sentinel from 'viewers'.
      viewers = (doc.viewers || []).filter(function(v) {
        return v !== '*';
      });
      annotators = doc.annotators || [];
      editors = doc.editors || [];
    }

    var allUserIds = [].concat(owners, writers, viewers, annotators, editors);

    var docId = doc._id;
    if (!doc._deleted) {
      // if there have been changes, we need to be ensure we are the admin
      if (
        hasMutated('owners') ||
        hasMutated('writers') ||
        hasMutated('viewers') ||
        hasMutated('annotators') ||
        hasMutated('editors')
      ) {
        requireAdmin();
      }

      if (
        (objectTypeMatches('MPLibraryCollection') ||
          objectTypeMatches('MPLibrary')) &&
        hasMutated('category')
      ) {
        throw { forbidden: 'category cannot be mutated' };
      }

      // only do this for non-deleted items for perf.
      var userIds = {};
      for (var i = 0; i < allUserIds.length; i++) {
        if (allUserIds[i] in userIds) {
          // prettier-ignore
          throw({ forbidden: 'duplicate userId:' + allUserIds[i] });
        }
        userIds[allUserIds[i]] = true;
      }

      // if the doc was _deleted, and this was the only doc granting access to
      // the channels then we don't want to grant access anymore.
      // e.g. User_A should no longer have access to User_B-profile channel.
      for (var j = 0; j < allUserIds.length; j++) {
        var ids = [];
        for (var k = 0; k < allUserIds.length; k++) {
          if (j === k) continue;
          // push() seems to be the best option for 'otto'.
          // https://github.com/robertkrimen/otto/blob/master/builtin_array.go
          ids.push(allUserIds[k] + '-read');
        }
        access(allUserIds[j], ids);

        var containersChannelID;
        if (objectTypeMatches('MPProject')) {
          containersChannelID = allUserIds[j] + '-projects';
        } else if (objectTypeMatches('MPLibraryCollection')) {
          containersChannelID = allUserIds[j] + '-library-collections';
        } else if (objectTypeMatches('MPLibrary')) {
          containersChannelID = allUserIds[j] + '-libraries';
        }

        if (containersChannelID) {
          channel(containersChannelID);
          access(allUserIds[j], containersChannelID);
        }
      }
    }

    var rChannelName = docId + '-read';
    var rwChannelName = docId + '-readwrite';
    /* The -bibitems channel is used to allow syncing only bibliography data associated with a container */
    var bibReadChannelName = docId + '-bibitems';
    var ownerChannelName = docId + '-owner';
    var annotatorChannelName = docId + '-annotator';
    var editorChannelName = docId + '-editor';
    /* The -metadata channel is used to allow syncing only manuscript metadata associated with a container */
    var metadataChannelName = docId + '-metadata';

    if (oldDoc) {
      requireAccess(rwChannelName);
    } else {
      // no oldDoc (it is a new document)
      try {
        requireAdmin();
      } catch (e) {
        // we are not the admin
        if (objectTypeMatches('MPLibraryCollection')) {
          requireUser([].concat(owners, writers));
        } else if (
          owners.length === 1 &&
          writers.length === 0 &&
          viewers.length === 0 &&
          annotators.length === 0 &&
          editors.length === 0
        ) {
          // this will throw if the current user isn't the user in the zeroth
          // element of the owners array.
          requireUser(owners[0]);
        } else {
          // throw that we are not the admin
          // prettier-ignore
          throw(e);
        }
      }
    }

    channel([rChannelName, rwChannelName]);
    access(owners, [
      rChannelName,
      rwChannelName,
      ownerChannelName,
      bibReadChannelName,
      metadataChannelName,
    ]);
    access(writers, [
      rChannelName,
      rwChannelName,
      bibReadChannelName,
      metadataChannelName,
    ]);
    access(viewers, [rChannelName, bibReadChannelName, metadataChannelName]);
    access(annotators, [
      rChannelName,
      bibReadChannelName,
      metadataChannelName,
      annotatorChannelName,
    ]);
    access(editors, [
      rChannelName,
      bibReadChannelName,
      metadataChannelName,
      editorChannelName,
    ]);
  } else if (
    objectTypeMatches('MPCollaboration') ||
    objectTypeMatches('MPInvitation') ||
    objectTypeMatches('MPContainerInvitation')
  ) {
    var isCollaboration = objectTypeMatches('MPCollaboration');
    var invitingUserID =
      doc.invitingUserID || (oldDoc && oldDoc.invitingUserID);

    if (isCollaboration) {
      if (hasMutated()) {
        // prettier-ignore
        throw({ forbidden: 'MPCollaboration is immutable' });
      }
      requireUser(invitingUserID);
    } else {
      // Updates to MPInvitation/MPContainerInvitation should only be from admin
      requireAdmin();
    }

    channel(invitingUserID);

    if (doc.invitedUserID) {
      channel(doc.invitedUserID);
      access(doc.invitedUserID, [invitingUserID + '-read']);
    }

    if (objectTypeMatches('MPContainerInvitation')) {
      var rChannelName = doc.containerID + '-read';
      channel([rChannelName]);
    }
  } else if (objectTypeMatches('MPContainerRequest')) {
    requireAdmin();

    var ownerChannelName = doc.containerID + '-owner';
    channel(ownerChannelName);
  } else if (objectTypeMatches('MPUserProfile')) {
    if (oldDoc === null || hasMutated('userID')) {
      requireAdmin();
    }
    rwChannelName = doc.userID + '-readwrite';
    requireAccess(rwChannelName);
    var profileChannelName = doc.userID + '-read';
    channel(rwChannelName);
    channel(profileChannelName);
    // Grant access to MPUserProfile channel
    access(doc.userID, doc._id + '-readwrite');
  } else if (objectTypeMatches('MPPreferences')) {
    var username = doc._id.replace(/^MPPreferences:/, '');
    requireUser(username);
    channel(username);
  } else if (objectTypeMatches('MPMutedCitationAlert')) {
    if (hasMutated('userID') || hasMutated('targetDOI')) {
      requireAdmin();
    }

    var userID = doc.userID;
    requireUser(userID);

    var channelName = userID + '-citation-alerts';
    channel(channelName);
    access(userID, channelName);
  } else if (objectTypeMatches('MPCitationAlert')) {
    if (
      oldDoc === null ||
      hasMutated('userID') ||
      hasMutated('sourceDOI') ||
      hasMutated('targetDOI')
    ) {
      requireAdmin();
    }

    var userID = doc.userID;

    if (hasMutated('isRead')) {
      requireUser(userID);
    }

    var channelName = userID + '-citation-alerts';
    channel(channelName);
    access(userID, channelName);
  } else if (objectTypeMatches('MPBibliographyItem')) {
    if (doc.keywordIDs) {
      for (var i = 0; i < doc.keywordIDs.length; i++) {
        var keywordID = doc.keywordIDs[i];
        var rChannel = keywordID + '-read';
        var rwChannel = keywordID + '-readwrite';

        requireAccess(rwChannel);
        channel([rChannel, rwChannel]);
      }
    }

    var channelName = doc.containerID + '-bibitems';
    channel(channelName);
  } else if (
    objectTypeMatches('MPCorrection') ||
    objectTypeMatches('MPCommentAnnotation') ||
    objectTypeMatches('MPManuscriptNote')
  ) {
    if (doc.contributions) {
      if (doc.contributions.length !== 1) {
        throw {
          forbidden: 'Only one contribution allowed',
        };
      }

      if (hasMutated('contributions')) {
        throw { forbidden: 'contributions cannot be mutated' };
      }

      var annotatorChannelName = doc.containerID + '-annotator';
      var editorChannelName = doc.containerID + '-editor';
      var rwChannelName = doc.containerID + '-readwrite';
      var contributorChannelName =
        doc.contributions[0].profileID + '-readwrite';

      var isApprovingCorrections = !!(
        objectTypeMatches('MPCorrection') &&
        oldDoc &&
        hasMutated('status')
      );
      var isResolvingComments = !!(
        (objectTypeMatches('MPCommentAnnotation') ||
          objectTypeMatches('MPManuscriptNote')) &&
        oldDoc &&
        hasMutated('resolved')
      );
      var isRejecting =
        (isApprovingCorrections &&
          doc.status &&
          doc.status.label === 'rejected') ||
        (isResolvingComments && doc.resolved === false);

      if (isRejecting) {
        requireAccess([
          contributorChannelName,
          editorChannelName,
          rwChannelName,
        ]);
      } else if (isApprovingCorrections || isResolvingComments) {
        requireAccess([editorChannelName, rwChannelName]);
      } else {
        /* If the current user has access to the channel of the profile in the
        contributions field, then the user must be the owner of the object */
        requireAccess(contributorChannelName);
        requireAccess([annotatorChannelName, editorChannelName, rwChannelName]);
      }
    }
    if (hasMutated('readBy')) {
      if (doc.readBy.includes(doc.userID)) {
        requireAccess([rwChannelName]);
      } else {
        throw {
          forbidden:
            'User can set status "read" only for himself and cannot unset it',
        };
      }
    }
  } else if (
    objectTypeMatches('MPManuscript') ||
    objectTypeMatches('MPContributor')
  ) {
    var channelName = doc.containerID + '-metadata';
    channel(channelName);
  } else if (objectTypeMatches('MPCommit')) {
    var annotatorChannelName = doc.containerID + '-annotator';
    var editorChannelName = doc.containerID + '-editor';
    var rwChannelName = doc.containerID + '-readwrite';

    if (!hasMutated()) {
      requireAccess([rwChannelName, annotatorChannelName, editorChannelName]);
    } else {
      requireAccess(rwChannelName);
    }
  }

  var containerId = doc.containerID;

  if (
    containerId &&
    !objectTypeMatches('MPContainerInvitation') &&
    !objectTypeMatches('MPContainerRequest')
  ) {
    var rChannelName = containerId + '-read';
    var rwChannelName = containerId + '-readwrite';
    if (
      !objectTypeMatches('MPCorrection') &&
      !objectTypeMatches('MPCommentAnnotation') &&
      !objectTypeMatches('MPManuscriptNote') &&
      !objectTypeMatches('MPCommit')
    )
      requireAccess(rwChannelName);
    channel([rChannelName, rwChannelName]);
  }
}

// this is how typescript generates exports.
// they work for JS and TS (i.e. with .d.ts file).
Object.defineProperty(exports, '__esModule', { value: true });
exports.syncFn = syncFn;
