const gulp = require('gulp')
const assert = require('assert')
const ejs = require('gulp-ejs')
const fs = require('fs')
const {
  manuscriptsSyncFn,
  derivedDataSyncFn,
  discussionsSyncFn,
} = require('@manuscripts/manuscripts-sync')

if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'test'
}

const getVersionPath = __dirname + '/bin/get-version.sh'

const revision = fs.existsSync(getVersionPath)
  ? require('child_process')
      .execSync(getVersionPath)
      .toString()
      .trim()
  : 'unknown-version'

const config = {}
Object.keys(process.env)
  .filter(
    k => k.match(/^APP_/) || k.match(/^NODE_ENV$/) || k.match(/NPM_TOKEN$/)
  )
  .forEach(k => (config[k.toLowerCase()] = process.env[k]))

config.app_test_action = config.app_test_action || 'test'

if (!config.app_initialize) {
  config.app_initialize = '0'
}

if (!config.app_run_after_initialize) {
  config.app_run_after_initialize = '0'
}

if (!config.app_version) {
  config.app_version = revision
}

if (!config.app_gateway_log_level) {
  config.app_gateway_log_level = 'warn'
}

if (!config.app_gateway_log_keys) {
  config.app_gateway_log_keys =
    process.env.NODE_ENV === 'production'
      ? '["HTTP"]'
      : '["HTTP", "Sync", "Attach", "Bucket"]'
}

gulp.task('validate-env', done => {
  for (const [key, value] of Object.entries(config)) {
    assert(value, `Missing ${key.toUpperCase()}`)
  }
  done()
})

gulp.task('compile-sync-gateway-config', () => {
  const ALLOWED_ENVS = ['test', 'production', 'development']
  if (!ALLOWED_ENVS.includes(config.node_env)) {
    throw new Error(
      `Expecting one of: [${ALLOWED_ENVS.join(', ')}] as NODE_ENV`
    )
  }

  return gulp
    .src('./docker/utils/templates/sync-gateway-config.json.ejs')
    .pipe(
      ejs(
        {
          ...config,
          manuscripts_sync_fn: JSON.stringify(manuscriptsSyncFn.toString()),
          derived_data_sync_fn: JSON.stringify(derivedDataSyncFn.toString()),
          discussions_sync_fn: JSON.stringify(discussionsSyncFn.toString()),
        },
        {},
        { ext: '' } // TODO: use gulp-rename for gulp-ejs v4
      )
    )
    .pipe(gulp.dest('./docker/sync_gateway'))
})

gulp.task('default', gulp.series('validate-env', 'compile-sync-gateway-config'))
