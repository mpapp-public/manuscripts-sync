// bind an admin port of a sync_gateway instance
// kubectl port-forward -n development development-syncgw-5d5d55b6bd-hj456 7001:4985

const { writeFileSync } = require('fs')
const { request } = require('http')

const BUCKET_NAME = 'projects'

function doReq(opts, body) {
  return new Promise((resolve, reject) => {
    const req = request(opts, res => {
      res.setEncoding('utf8')
      let chunks = ''

      res.on('data', chunk => {
        chunks += chunk
      })

      res.on('end', () =>
        resolve({
          body: JSON.parse(chunks),
          statusCode: res.statusCode,
        })
      )
    })

    req.on('error', e => {
      reject(e.message)
    })

    if (body) {
      req.end(body)
    } else {
      req.end()
    }
  })
}

async function putDoc(doc) {
  const payload = JSON.stringify(doc)

  const opts = {
    hostname: 'localhost',
    method: 'PUT',
    port: 7001, // non-standard to avoid confusion with local instance
    path: `/${BUCKET_NAME}/${doc._id}?rev=${doc._rev}`,
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(payload),
    },
  }

  const response = await doReq(opts, payload)

  if (response.statusCode === 201) {
    return [null, response.body]
  } else {
    return [response, null]
  }
}

async function getDocs() {
  const opts = {
    hostname: 'localhost',
    method: 'GET',
    port: 7001,
    path: `/${BUCKET_NAME}/_all_docs?include_docs=true`,
  }

  const response = await doReq(opts)

  if (response.statusCode === 200) {
    return response.body.rows
  } else {
    throw new Error('unable to fetch _all_docs')
  }
}

;(async function() {
  console.log('Fetching _all_docs')
  const docs = await getDocs()

  // write docs for debugging purposes
  writeFileSync('_all_docs.json', JSON.stringify(docs, null, 2), 'utf8')

  const docsToTouch = docs.map(x => x.doc)
  // .filter(x => x.objectType === 'MPUserProfile' && !x.createdAt)
  // .map(x => {
  //   const ts = Math.floor(Date.now() / 1000)
  //   x.createdAt = ts
  //   x.updatedAt = ts
  //   return x
  // })

  const success = []
  const fail = []

  for (const doc of docsToTouch) {
    const [err, body] = await putDoc(doc)

    if (err) {
      fail.push({ err, doc })
    } else {
      success.push(body)
    }

    console.log('success:', success.length, '  fail:', fail.length)
  }

  console.log(
    require('util').inspect(
      { success, fail },
      { maxArrayLength: null, depth: null }
    )
  )
})()
